//
// OPDRACHT
//
// 1
// VOEG VIA JAVASCRIPT EEN VIS EN EEN BUBBLE TOE
// ZET DE VIS OP EEN WILLEKEURIGE PLEK IN HET SCHERM MET EEN WILLEKEURIGE KLEUR
// ZET DE BUBBLE OP EEN WILLEKEURIGE X POSITIE

// 2
// MAAK EEN FOR LOOP DIE 50 VISJES EN BUBBLES TOEVOEGT. DEZE MOETEN ALLEMAAL ANDERS ZIJN!

// 3
// GEBRUIK NU SETTIMOUT OF SETINTERVAL OM NIEUWE VISJES EN BUBBLES TE PLAATSEN

// 4
// PLAATS EEN TITEL EN START KNOP. ALS JE OP START KLIKT VERDWIJNEN DE TITEL EN KNOP, EN 
// DAARNA WORDEN PAS DE VISJES GETEKEND

// 5 
// HANG EEN CLICK EVENT LISTENER AAN ELK VISJE. ALS GEKLIKT WORDT
// GEEF JE DE GEKLIKTE VIS EEN NIEUWE CLASS DIE EEN ANDERE ACHTERGROND HEEFT 
// fish.classList.add(".deadfish");

function startGame(){
    var makeFish = setInterval( function(){
        generateObjects(makeFish);           
    }, 1000 );   
}

function generateObjects(makeFish){
    var objects = countObjects();
    
    if(objects === 75){
        clearInterval(makeFish);
    } else {
        if(isEven(objects) === true){
            createObject("fish", true, objects);    
        } else {
            createObject("bubble", false, objects);
        }        
    }
}

function countObjects(){
    var countFish = document.getElementsByTagName("fish").length;
    var countBubble = document.getElementsByTagName("bubble").length;
    
    total = countFish + countBubble;
    
    return total;
}

function isEven(value) {
    return (value%2 == 0);
}

//
// start the game on window load
//
function createObject(kind, colorSwitch, objects){
    var body = document.getElementsByTagName("body")[0];
    var xPos = Math.floor(Math.random() * 1500);
    var yPos = Math.floor(Math.random() * 750);
    var newObject = document.createElement(kind);
    
    var i = objects;
    newObject.classList.add("obj"+ i);
    
    if(colorSwitch === true){
        var color = Math.floor((Math.random() * 360) + 1);
        newObject.style.webkitFilter = "hue-rotate("+ color + "deg)";
        newObject.style.filter = "hue-rotate("+ color + "deg)";
        
        // Three kinds of fish animations
        var rnd = Math.floor((Math.random() * 3)+1);
        newObject.classList.add("fish" + rnd);    
    }
    
    newObject.style.left = xPos+"px";
    newObject.style.top = yPos+"px";
    
    newObject.addEventListener("click", onClick);
    body.appendChild(newObject);
}

function onClick(event){
    var source = event.target || event.srcElement;

    var numb = source.classList[0].replace(/\D/g,'');

    if(isEven(numb)){
        var str = source.classList[1];
        
        source.classList.remove(str);
        source.classList.add("deadfish");   
    }     
}

window.addEventListener("load", function () {
    startGame();
});
